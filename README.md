# [deprecated] circles-intersect

Moved to codeberg

Checks if two circles intersect.

# Usage

[![NPM](https://nodei.co/npm/circles-intersect.png)](https://www.npmjs.com/package/circles-intersect)

### `circlesIntersect(x1, y1, r1, x2, y2, r2)`

where x1, y1 ,x2 and y2 the center points of each circle
and r1, r2 their radius

```
let circlesIntersect = require('circles-intersect')

let intersects = circlesIntersect(15,20,3,10,14,5)
console.log(intersects) // true
```

## License

MIT
