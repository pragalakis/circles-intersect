module.exports = function(x1, y1, r1, x2, y2, r2) {
  if (r1 <= 0 || r2 <= 0) throw 'Radius must be bigger than 0.';
  const dist = Math.hypot(x2 - x1, y2 - y1);
  return dist <= r1 + r2 ? true : false;
};
